/*
 * Copyright 2018 luis.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.luis.pdf.merger.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author luis
 */
public class Arguments {

    Map<String, List<String>> arguments = new HashMap<>();

    public Arguments(String[] args) {
        parse(args);
    }

    /**
     * Load all start parameters mapping they.
     *
     * @param args
     */
    private void parse(String[] args) {

        arguments = new HashMap<>();
        String lastKey = null;
        for (String s : args) {
            if (s.startsWith("-") && !isNumber(s)) {
                lastKey = s.substring(1);
                addKey(lastKey);
            } else if (lastKey != null) {
                addKey(lastKey, s);
            } else {
                // No start param
                lastKey = s;
                addKey(lastKey);
            }
        }
    }

    private void addKey(String key) {
        if (!arguments.containsKey(key)) {
            arguments.put(key, new ArrayList<>());
        }
    }

    private void addKey(String key, String val) {

        if (!arguments.containsKey(key)) {
            arguments.put(key, new ArrayList<>());
        }

        arguments.get(key).add(val);
    }

    public boolean is(String... keys) {
        if (keys == null || keys.length == 0) {
            return false;
        }

        for (String key : keys) {
            var vals = arguments.get(key);
            if (vals != null) {
                return vals.isEmpty() || !vals.get(0).equalsIgnoreCase("false");
            }
        }

        return false;
    }

    public String getStr(String... keys) {

        if (keys == null || keys.length == 0) {
            return "";
        }

        for (String key : keys) {
            List<String> vals = arguments.get(key);
            if (vals != null && !vals.isEmpty()) {
                return vals.get(0);
            }
        }

        return "";
    }

    public <A> A get(Class<A> type, String... keys) {

        if (keys == null || keys.length == 0) {
            return null;
        }

        for (String key : keys) {
            var vals = arguments.get(key);
            if (vals != null && !vals.isEmpty()) {
                return parse(type, vals.get(0));
            }
        }

        return null;
    }

    public List<String> getList(String... keys) {

        if (keys == null || keys.length == 0) {
            return new ArrayList<>();
        }

        for (String key : keys) {
            List<String> vals = arguments.get(key);
            if (vals != null && !vals.isEmpty()) {
                return vals;
            }
        }

        return new ArrayList<>();
    }

    /**
     * Check if the given string is a number.
     *
     * @param val
     * @return
     */
    private static boolean isNumber(String val) {
        try {
            Double.valueOf(val);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isEmpty() {
        return arguments == null || arguments.isEmpty();
    }

    private <A> A parse(Class<A> aClass, String val) {
        if (aClass.equals(Integer.class) || aClass.equals(int.class)) {
            return (A) Integer.valueOf(val);
        } else if (aClass.equals(Long.class) || aClass.equals(long.class)) {
            return (A) Long.valueOf(val);
        } else if (aClass.equals(Double.class) || aClass.equals(double.class)) {
            return (A) Double.valueOf(val);
        } else if (aClass.equals(Float.class) || aClass.equals(Float.class)) {
            return (A) Float.valueOf(val);
        } else if (aClass.equals(Short.class) || aClass.equals(Short.class)) {
            return (A) Short.valueOf(val);
        } else if (aClass.equals(Byte.class) || aClass.equals(Byte.class)) {
            return (A) Byte.valueOf(val);
        }
        throw new IllegalStateException("Can't parse the value " + val + " to type: " + aClass);
    }
}

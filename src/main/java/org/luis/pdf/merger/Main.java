/*
 * Copyright 2018 luis.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.luis.pdf.merger;

import java.util.List;
import org.luis.pdf.merger.util.Arguments;
import org.luis.pdf.merger.util.MessageDTO;

/**
 *
 * @author luis
 */
public class Main {

    public static Arguments arguments;

    public static void main(String[] args) {

        arguments = new Arguments(args);

        if (arguments.isEmpty() || arguments.getList("input", "i").isEmpty()) {
            printUsageAndExit();
        }

        var merger = new Merger();
        merger.setArguments(arguments);

        var msgs = merger.merge();
        var hasError = false;
        for (MessageDTO msg : msgs) {
            if (msg.isError()) {
                print("ERROR: " + msg.getMessage());
                hasError = true;
            } else {
                print(msg.getMessage());
            }
        }

        if (hasError) {
            System.exit(1);
        }

    }

    public static void printUsageAndExit() {
        print("Usage: ");
        print("pdf-merger.jar [options]");
        print("Options: ");
        print("\t   -input -i: (Required) List of files to merge");
        print("\t   -input -o: Name of output file, default: ");
        print("Use the java \"Xmx\" parameter to limit memory usage.");
        print("Example:");
        print("\tjava -Xmx100m -jar pdf-merger -i /path/input/file1.pdf /path/input/file2.pdf /path/input/file3.pdf -o /path/output/out.pdf");
        System.exit(0);
    }

    public static void print(String val) {
        System.out.println(val);
    }
}

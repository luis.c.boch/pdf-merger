/*
 * Copyright 2018 luis.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.luis.pdf.merger;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.BadPdfFormatException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfSmartCopy;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.luis.pdf.merger.util.Arguments;
import org.luis.pdf.merger.util.MessageDTO;

/**
 *
 * @author luis
 */
public class Merger {

    private Arguments arguments;

    private String outFile = "";

    public Merger() {
    }

    public String getOutFile() {
        return outFile;
    }

    public Merger setOutFile(String outFile) {
        this.outFile = outFile;
        return this;
    }

    public Merger setArguments(Arguments arguments) {
        this.arguments = arguments;
        return this;
    }

    public Arguments getArguments() {
        return arguments;
    }

    public List<MessageDTO> merge() {

        long ms = System.currentTimeMillis();

        var rs = new ArrayList<MessageDTO>();

        try {
            File outFile = new File(arguments.getStr("output", "o"));
            FileOutputStream out = new FileOutputStream(outFile);
            // Default output file
            Document document = new Document(PageSize.A4);
            PdfCopy copy = new PdfSmartCopy(document, out);
            document.open();

            List<String> inputFiles = arguments.getList("input", "i");
            Collections.sort(inputFiles);

            for (String from : inputFiles) {
                copy(from, copy);
            }

            copy.close();
            document.close();
            out.close();

            rs.add(new MessageDTO("Success with: " + (System.currentTimeMillis() - ms) + "ms -> " + outFile.getPath(), false));
            return rs;
        } catch (Exception e) {
            e.printStackTrace(System.out);
            rs.add(new MessageDTO("Error when merge is applied: " + e.getMessage()));
            return rs;
        } finally {
        }
    }

    private void copy(String from, PdfCopy copy) throws IOException, BadPdfFormatException {
        PdfReader reader = new PdfReader(from);
        Main.print("-" + from + " (" + reader.getNumberOfPages() + " pgs)");

        for (int i = 0; i < reader.getNumberOfPages(); i++) {
            PdfImportedPage ip = copy.getImportedPage(reader, i + 1);
            copy.addPage(ip);
        }

        reader.close();
    }

}
